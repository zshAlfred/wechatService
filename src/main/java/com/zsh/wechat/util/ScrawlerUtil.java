package com.zsh.wechat.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zsh.wechat.constants.ConstVariableForRedis;
import com.zsh.wechat.entity.FileListBean;
import com.zsh.wechat.entity.ModeConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import javax.annotation.Resource;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;


/**
 * Created by admin on 2017/6/19.
 */
@Component
public class ScrawlerUtil {

    private static final Logger logger = LoggerFactory.getLogger(ScrawlerUtil.class);

//    @Resource(name="modeConfig")
//    private ModeConfig modeConfig;

    public FileListBean getMaterialFileListBean(){

        FileListBean fileListBean=null;
        try{
            long start = System.currentTimeMillis();
            String result = getAjaxContent(ConstVariableForRedis.FileListUrl);

//            if(modeConfig.getDebugModeEnable()!=null&&modeConfig.getDebugModeEnable().equalsIgnoreCase("true"))
//            {
//                logger.info("The content grabbed from web is : \n\n"+result);
//            }

            String jsonString=result.substring(result.indexOf("var file_list =")+15,result.lastIndexOf("</pre></body></html>"));

            ObjectMapper objectMapper=new ObjectMapper();
            fileListBean = objectMapper.readValue(jsonString,FileListBean.class);

            long end = System.currentTimeMillis();
            logger.info("===============The time consumed for grab the content from web is ：" + (end - start) + "===============");
        }
        catch (Exception e)
        {
            logger.error("Exception caught when try to get file links from web and parse it to json object.\n\n "+e.getMessage()+"\n\n"+e.getStackTrace());
        }

        return fileListBean;
    }

    private String getAjaxContent(String url) throws Exception {
        Runtime rt = Runtime.getRuntime();
        String scrawlerPath=String.format("%s\\phantomjs.exe %s\\Scrawler.js %s",ConstVariableForRedis.ScrawlerPath,ConstVariableForRedis.ScrawlerPath,url);
        Process p = rt.exec(scrawlerPath);
        InputStream is = p.getInputStream();
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        StringBuffer sbf = new StringBuffer();
        String tmp = "";
        while((tmp=br.readLine())!=null) {
            sbf.append(tmp + "\n");
        }
        return sbf.toString();
    }
}

