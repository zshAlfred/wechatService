package com.zsh.wechat.util;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.CountDownLatch;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;


/**
 * Created by admin on 2017/6/12.
 */
@Service
public class BigFileDownloadManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(BigFileDownloadManager.class);

    /**
     *
     * 每个线程下载的字节数
     */
    private long unitSize = 10000 * 1024;
    private TaskExecutor taskExecutor;
    private CloseableHttpClient httpClient;

    @Value("${image_file.dir}")
    private String downloadedZipDir;

    public BigFileDownloadManager() {
        PoolingHttpClientConnectionManager cm = new PoolingHttpClientConnectionManager();
        cm.setMaxTotal(100);
        httpClient = HttpClients.custom().setConnectionManager(cm).build();

        taskExecutor=getTaskExecutor();
    }

    /**
     *
     * 启动多个线程下载文件
     */
    //@Test
    public boolean  doDownload(String fileURL) throws IOException {
        // String remoteFileUrl="http://123.206.208.145/screenshot/screenshot_2017061217_9.zip";
        String remoteFileUrl=fileURL;
        //String localPath="D://newScreenShot//";
        String localPath=downloadedZipDir;

        String fileName = new URL(remoteFileUrl).getFile();

        System.out.println("远程文件名称："+fileName);

        fileName = fileName.substring(fileName.lastIndexOf("/") + 1,
                fileName.length()).replace("%20", " ");

        System.out.println("本地文件名称："+fileName);
        long fileSize = this.getRemoteFileSize(remoteFileUrl);

        this.createFile(localPath+System.currentTimeMillis()+fileName, fileSize);

        Long threadCount = (fileSize/unitSize)+(fileSize % unitSize!=0?1:0);
        long offset = 0;

        CountDownLatch end = new CountDownLatch(threadCount.intValue());

        if (fileSize <= unitSize) {// 如果远程文件尺寸小于等于unitSize
            BigFileDownload downloadThread = new BigFileDownload(remoteFileUrl,
                    localPath+fileName, offset, fileSize,end,httpClient);
            taskExecutor.execute(downloadThread);
        } else {// 如果远程文件尺寸大于unitSize
            for (int i = 1; i < threadCount; i++) {
                BigFileDownload downloadThread = new BigFileDownload(remoteFileUrl,
                        localPath+fileName, offset, unitSize,end,httpClient);
                taskExecutor.execute(downloadThread);
                offset = offset + unitSize;
            }
            if (fileSize % unitSize != 0) {// 如果不能整除，则需要再创建一个线程下载剩余字节
                BigFileDownload downloadThread = new BigFileDownload(remoteFileUrl, localPath+fileName, offset, fileSize - unitSize * (threadCount-1),end,httpClient);
                taskExecutor.execute(downloadThread);
            }
        }
        try {
            end.await();
        } catch (InterruptedException e) {
            LOGGER.error("DownLoadManager exception msg:{}",ExceptionUtils.getFullStackTrace(e));
            e.printStackTrace();
        }
		System.out.println("下载完成");
        return true;
    }

    /**
     *
     * 获取远程文件尺寸
     */

    private long getRemoteFileSize(String remoteFileUrl) throws IOException {

        long fileSize = 0;

        HttpURLConnection httpConnection = (HttpURLConnection) new URL(remoteFileUrl).openConnection();
        httpConnection.setRequestMethod("HEAD");
        int responseCode = httpConnection.getResponseCode();

        if (responseCode >= 400) {
            LOGGER.debug("Web服务器响应错误!");
            return 0;
        }

        String sHeader;
        for (int i = 1;; i++) {
            sHeader = httpConnection.getHeaderFieldKey(i);
            if (sHeader != null && sHeader.equals("Content-Length")) {
                System.out.println("文件大小ContentLength:"+ httpConnection.getContentLength());
                fileSize = Long.parseLong(httpConnection.getHeaderField(sHeader));
                break;
            }
        }
        return fileSize;
    }
    /**
     *
     * 创建指定大小的文件
     */
    private void createFile(String fileName, long fileSize) throws IOException {
        File newFile = new File(fileName);
        RandomAccessFile raf = new RandomAccessFile(newFile, "rw");
        raf.setLength(fileSize);
        raf.close();
    }


    public TaskExecutor getTaskExecutor() {
        ThreadPoolTaskExecutor d=new ThreadPoolTaskExecutor();
        d.setCorePoolSize(5);//线程池维护线程的最少数量
        d.setMaxPoolSize(10);//线程池维护线程的最大数量
        d.setQueueCapacity(1000);//线程池所使用的缓冲队列
        d.initialize();
        taskExecutor=d;
        return taskExecutor;
    }

    public void setTaskExecutor(TaskExecutor taskExecutor) {
        this.taskExecutor = taskExecutor;
    }
}
