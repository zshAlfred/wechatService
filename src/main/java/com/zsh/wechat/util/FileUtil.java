package com.zsh.wechat.util;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.ArrayUtils;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Set;

/**
 * Created by xinghao1 on 2017/6/12.
 */
public class FileUtil {

    public static List<File> filterPictures(String sourceDir) {
        File dir = new File(sourceDir);
        File[] files = dir.listFiles((dir1, name) -> name.endsWith(".jpg"));

        if (ArrayUtils.isEmpty(files)) {
            return Collections.emptyList();
        }
        return Lists.newArrayList(files);
    }
}
