package com.zsh.wechat.model;

import org.opencv.core.Mat;

/**
 * Created by xinghao1 on 2017/7/8.
 */
public class CheatingTemplate {
    private String name;
    private Mat template;
    private int threshold;

    public CheatingTemplate(String name, Mat template) {
        this.name = name;
        this.template = template;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Mat getTemplate() {
        return template;
    }

    public void setTemplate(Mat template) {
        this.template = template;
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }

    @Override
    public String toString() {
        return "CheatingTemplate{" +
            "name='" + name + '\'' +
            ", threshold=" + threshold +
            '}';
    }
}
