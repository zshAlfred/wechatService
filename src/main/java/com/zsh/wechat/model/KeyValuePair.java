package com.zsh.wechat.model;

import java.util.Map;

public class KeyValuePair<K, V> implements Map.Entry<K, V> {

    private final K key;
    private V value;

    private KeyValuePair(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public static <K, V> KeyValuePair<K, V> of(K k1, V v1) {
        return new KeyValuePair<>(k1, v1);
    }

    @Override
    public K getKey() {
        return key;
    }

    @Override
    public V getValue() {
        return value;
    }

    @Override
    public V setValue(V value) {
        V old = this.value;
        this.value = value;
        return old;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof KeyValuePair) {
            KeyValuePair other = (KeyValuePair) obj;
            return other.hashCode() == this.hashCode();
        }

        return false;
    }

    @Override
    public String toString() {
        return "[ key=" + key + " value=" + value + "]";
    }

    @Override
    public int hashCode() {
        return key.hashCode() * 100 + value.hashCode() * 10;
    }
}
