package com.zsh.wechat.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException {
    private String param;

    public ResourceNotFoundException(String param) {
        this.param = param;
    }

    @Override
    public String getMessage() {
        return "Unable to find resource with search condition: " + param;
    }
}