package com.zsh.wechat.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.UNAUTHORIZED)
public class ValidationException extends RuntimeException {
    private String token;

    public ValidationException(String token) {
        this.token = token;
    }
}