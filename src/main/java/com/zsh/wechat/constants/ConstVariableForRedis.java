package com.zsh.wechat.constants;

/**
 * Created by Lenovo on 7/16/2017.
 */
public class ConstVariableForRedis {
    public static final String FileURL="http://123.206.208.145/screenshot/";
    public static final String RedisKeyUnhandledLinkList="UnHandledLinkList";
    public static final String RedisKeyUnHandledLinkSet="UnHandledLinkSet";
    public static final String RedisKeyUnHandledLinkFlag="UnHandledLinkFlag";
    public static final String RedisKeySuccessDownloadSet="SuccessDownloadSet";
    public static final String RedisKeySuccessDownloadList="SuccessDownloadList";
    public static final String RedisKeyFailedDownloadList="FailedDownloadList";
    public static final String RedisKeyFailedDownloadSet="FailedDownloadSet";
    public static final String DownloadZipFileFolder="D:\\newScreenShot";
    public static final String UnZippedPicturePool="D:\\newScreenShot\\bak";
    public static final String FileListUrl="http://123.206.208.145/screenshot/file_lsit.js";
    public static final String ScrawlerPath="C:\\WorkBench\\proj\\wechatService\\src\\main\\resources\\ScrawlerTool";

}
