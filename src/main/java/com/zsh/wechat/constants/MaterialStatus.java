package com.zsh.wechat.constants;

/**
 * Created by admin on 2017/6/19.
 */
public class MaterialStatus {

    public static final int NOT_DOWNLOADED=1;

    public static final int DOWNLOADING=2;

    public static final int DOWNLOADED_BUT_NOT_UNZIPPED=3;

    public static final int UNZIPPING=4;

    public static final int UNZIPPED_BUT_NOT_DELETED=5;

    public static final int DELETED=6;
}
