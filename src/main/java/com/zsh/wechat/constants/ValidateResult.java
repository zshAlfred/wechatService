package com.zsh.wechat.constants;

public enum ValidateResult {
    SUCCESS(10, "validation pass"),
    FAIL(20, "validation fail"),
    UNAUTHORIZED(23, "unauthorized"),
    EXCEPTION(22, "exception occurred while validate"),;

    private int status;
    private String message;

    ValidateResult(int status, String message) {
        this.status = status;
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
