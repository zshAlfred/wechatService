package com.zsh.wechat.constants;

import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * Created by xinghao1 on 2017/7/17.
 */
@Component
public class RedisKeys {
    //等待处理的图片列表
    public String waitingList = "pics:waiting_list";

    //系统自动识别的作弊图片
    public String systemDetectedCheatingList = "pics:level1_list";

    //人工识别的作弊图片
    public String manualDetectedCheatingList = "pics:level2_list";

    @Autowired
    public RedisKeys(
        @Autowired Environment env,
        @Value("${redis_keys.waiting_list}") String waitingList,
        @Value("${redis_keys.level1_list}") String systemDetectedCheatingList,
        @Value("${redis_keys.manual_detected_cheating_list}") String manualDetectedCheatingList) {

        String[] activeProfs = env.getActiveProfiles();
        if (ArrayUtils.isNotEmpty(activeProfs)) {
            this.waitingList = activeProfs[0] + ":" + waitingList;
            this.systemDetectedCheatingList = activeProfs[0] + ":" + systemDetectedCheatingList;
            this.manualDetectedCheatingList = activeProfs[0] + ":" + manualDetectedCheatingList;
        }
    }
}
