package com.zsh.wechat.message;

import com.alibaba.fastjson.JSON;
import com.zsh.wechat.constants.RedisKeys;
import com.zsh.wechat.entity.MatchResult;
import com.zsh.wechat.entity.Pic;
import com.zsh.wechat.service.CheatingProcessor;
import com.zsh.wechat.service.HotConfService;
import com.zsh.wechat.service.RedisService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

/**
 * Created by xinghao1 on 2017/7/14.
 */
@Service
public class DetectTaskConsumer {

    private static final Logger logger = LoggerFactory.getLogger(DetectTaskConsumer.class);
    @Autowired
    RedisService redisService;

    @Autowired
    CheatingProcessor cheatingProcessor;

    private ThreadPoolTaskExecutor executor;

    @Autowired
    HotConfService hotConfService;

    @Autowired
    RedisKeys redisKeys;

    private static int CORE_POOL_SIZE = 1;
    private static int MAXIMUM_POOL_SIZE = 4;
    private static int KEPP_ALIVE_TIME = 600;
    private static int TASK_QUEUE_SIZE = 1000;

//    private static ThreadPoolExecutor executor = new ThreadPoolExecutor(CORE_POOL_SIZE, MAXIMUM_POOL_SIZE, KEPP_ALIVE_TIME,
//        TimeUnit.SECONDS, new LinkedBlockingQueue<>(TASK_QUEUE_SIZE), new RejectedExecutionHandler() {
//        @Override
//        public void rejectedExecution(Runnable runnable, ThreadPoolExecutor executor) {
//            logger.error("消费速度太慢");
//        }
//    }
//    );


    //如果队列中没有新进入的图片，会阻塞住，直到有新任务进队列
    public Pic getNewPic() {
        try {
            String strInfo = redisService.blpop(redisKeys.waitingList);
            return JSON.parseObject(strInfo, Pic.class);
        } catch (Exception e) {
            logger.error("exception while get new pic", e);
            throw e;
        }
    }
//
//    public void consumeUsingThreadPool() {
//        executor.execute(() -> {
//            MatchResult result = cheatingProcessor.detect(getNewPic(), hotConfService.getThreshold());
//            cheatingProcessor.processResult(result);
//        });
//    }

    public void consume() throws InterruptedException {
        while (true) {
            Pic newPic = getNewPic();
            MatchResult result = cheatingProcessor.detect(newPic.getFullPath(), hotConfService.getThreshold());
            cheatingProcessor.processResult(result);
        }
    }
}
