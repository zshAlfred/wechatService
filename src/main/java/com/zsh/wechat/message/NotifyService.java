package com.zsh.wechat.message;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.context.annotation.Profile;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.stereotype.Service;

//@Service
//@EnableBinding(NotifyService.MessageQueueSink.class)
//@Profile("!offline")
public class NotifyService {


    @CacheEvict(value = {"usuallyThisIsAMethodNameAsCacheName"},
        beforeInvocation = true, allEntries = true)
    @StreamListener(MessageQueueSink.WECHAT_SERVICE)
    public void clean() {
        String temp = "";
    }

    public String testMethod(String key) {
        return key;
    }

    public interface MessageQueueSink {

        String WECHAT_SERVICE = "wechat_service_clean_cache";

        @Input(WECHAT_SERVICE)
        SubscribableChannel cleanCache();
    }
}
