package com.zsh.wechat.entity;

import java.util.List;

/**
 * Created by admin on 2017/6/19.
 */
public class FileListBean {

    private String filelist;

    private String filesize;

    public String getFilelist() {
        return filelist;
    }

    public String getFilesize() {
        return filesize;
    }

    public void setFileList(String filelist) {
        this.filelist = filelist;
    }

    public void setFilesize(String filesize) {
        this.filesize = filesize;
    }
}
