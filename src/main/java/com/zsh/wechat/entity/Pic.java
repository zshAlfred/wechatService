package com.zsh.wechat.entity;

import java.io.File;

public class Pic {
    private String id;
    private String picname;
    private String fullPath;
    private String picPackage;//来自哪个zip包
    private boolean needValidate = true;//是否需要二次人工验证
    private String creationDate;//截屏时间，即图片生成的时间
    private String validationDate;//系统验证为 可能作弊 的时间，未启用

    public Pic() {
    }

    public static Pic of(File file) {
        return new Pic(file.getName(), file.getAbsolutePath());
    }

    public Pic(String picname, String fullPath) {
        this.picname = picname;
        this.fullPath = fullPath;
    }

    public boolean isNeedValidate() {
        return needValidate;
    }

    public void setNeedValidate(boolean needValidate) {
        this.needValidate = needValidate;
    }

    public String getPicname() {
        return picname;
    }

    public void setPicname(String picname) {
        this.picname = picname;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(String creationDate) {
        this.creationDate = creationDate;
    }

    public String getValidationDate() {
        return validationDate;
    }

    public void setValidationDate(String validationDate) {
        this.validationDate = validationDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPicPackage() {
        return picPackage;
    }

    public void setPicPackage(String picPackage) {
        this.picPackage = picPackage;
    }

    public String getFullPath() {
        return fullPath;
    }

    public void setFullPath(String fullPath) {
        this.fullPath = fullPath;
    }
}
