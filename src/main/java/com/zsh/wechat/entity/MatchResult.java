package com.zsh.wechat.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.opencv.core.Point;

/**
 * Created by xinghao1 on 2017/7/3.
 */
public class MatchResult {
    private String source;
    private String target;

    private double percent;
    private Point point;
    private boolean match;


    public static final MatchResult NO_CHEATING = new MatchResult(false);

    public static MatchResult of(Point point, double percent) {
        return new MatchResult(point, percent);
    }

    public static MatchResult noMatch(String source) {
        MatchResult matchResult = new MatchResult(false);
        return matchResult.setSource(source);
    }

    public MatchResult(boolean match) {
        this.match = match;
    }

    public MatchResult(Point point, double percent) {
        this.point = point;
        this.percent = percent;
    }

    public boolean matchByThreshold(double threshold) {
        match = percent > threshold;
        return match;
    }

    public String getSource() {
        return source;
    }

    public MatchResult setSource(String source) {
        this.source = source;
        return this;
    }

    public String getTarget() {
        return target;
    }

    public MatchResult setTarget(String target) {
        this.target = target;
        return this;
    }

    @JsonIgnore
    public Point getPoint() {
        return point;
    }

    public double getPercent() {
        return percent;
    }

    public boolean isMatch() {
        return match;
    }

    public void setMatch(boolean match) {
        this.match = match;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    @Override
    public String toString() {
        return "MatchResult{" +
            "percent=" + percent +
            "match=" + match +
            ",source" + source +
            ",target" + target +
            '}';
    }
}
