package com.zsh.wechat.entity;


/**
 * Created by Lenovo on 8/13/2017.
 */

public class ModeConfig {

    private String debugModeEnable;

    public String getDebugModeEnable()
    {
        return this.debugModeEnable;
    }
    public void setDebugModeEnable(String debugModeEnable)
    {
        this.debugModeEnable=debugModeEnable;
    }
}
