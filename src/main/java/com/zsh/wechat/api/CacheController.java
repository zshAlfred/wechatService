package com.zsh.wechat.api;

import com.zsh.wechat.model.CheatingTemplate;
import com.zsh.wechat.service.CheatingProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by xinghao1 on 2017/6/12.
 */
@RequestMapping("/cache")
@RestController
public class CacheController {

    @Autowired
    CheatingProcessor cheatingProcessor;

    @Value("${images.cheating_templates}")
    private String cheatingTemplatePath;

    @PostConstruct
    public void init() {
        // TODO: 2017/7/18  自定义注解来解析相对路径和绝对路径
        if (cheatingTemplatePath.startsWith("classpath:")) {
            cheatingTemplatePath = getClass().getResource(cheatingTemplatePath.replace("classpath:", "")).getPath();
        }
    }

    @GetMapping("/reloadCheatingTemplates")
    public List<CheatingTemplate> reloadCheatingTemplates() {
        return cheatingProcessor.loadCheatingTemplates();
    }

    @PostMapping("/addCheatingTemplates")
    public List<CheatingTemplate> addCheatingTemplates(@RequestPart MultipartFile multipartFile) throws IOException {
        File file = new File(getClass().getResource(cheatingTemplatePath).getPath() + multipartFile.getOriginalFilename());
        // 转存文件
        multipartFile.transferTo(file);
        //todo 存放到固定目录
        return reloadCheatingTemplates();
    }
}
