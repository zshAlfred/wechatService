package com.zsh.wechat.api;

import com.zsh.wechat.constants.RedisKeys;
import com.zsh.wechat.entity.Pic;
import com.zsh.wechat.service.CheatingProcessor;
import com.zsh.wechat.service.RedisService;
import com.zsh.wechat.util.FileUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.util.List;

/**
 * Created by xinghao1 on 2017/7/17.
 */
@RequestMapping("/process")
@RestController
public class PicProcessController {

    @Autowired
    RedisService redisService;

    @Value("${image_file.dir}")
    private String newPicDir;

    @Value("${image_file.result_dir.wait_manual}")
    private String waitManualDir;

    @Autowired
    RedisKeys redisKeys;

    @Autowired
    CheatingProcessor processor;

    @GetMapping(value = "/manual/next_pic")
    public String getNextPic() {
        String pic = redisService.lpop(redisKeys.systemDetectedCheatingList);
        return pic;
    }

    @PostMapping(value = "/manual/uncheat")
    public boolean processUnCheat(@RequestHeader("current") String current) {
        return processor.processUnCheat(waitManualDir + current);
    }

    @PostMapping(value = "/manual/cheat")
    public boolean processCheat(@RequestHeader("current") String current) {
        return processor.confirmCheat(waitManualDir + current);
    }

    /**
     * 强行同步数据，将待处理图片写入redis
     * 正常情况下不要调用该接口
     */
    @GetMapping(value = "/sync/reload_new")
    public ResponseEntity<String> forceReload(@RequestHeader("password") String password) {
        if ("woqueren".equals(password)) {
            List<File> files = FileUtil.filterPictures(newPicDir);

            redisService.del(redisKeys.waitingList);
            long count = files.parallelStream()
                    .map(file -> new Pic(file.getName(), file.getAbsolutePath()))
                    .peek(pic -> redisService.rpush(redisKeys.waitingList, pic))
                    .count();
            return ResponseEntity.ok("加载了" + count + "张图片到待系统自动处理列表（redis）");
        }
        return ResponseEntity.ok("需要密码,放置误操作");
    }

}
