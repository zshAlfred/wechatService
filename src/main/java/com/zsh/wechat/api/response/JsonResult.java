package com.zsh.wechat.api.response;

import org.springframework.http.HttpStatus;

/**
 * Created by xinghao1 on 2017/6/11.
 */
public class JsonResult {
    private boolean status;
    private int code;
    private Object data;

    public JsonResult() {
    }

    public JsonResult(boolean status, int code, Object data) {
        this.status = status;
        this.code = code;
        this.data = data;
    }

    public static JsonResult success(Object data) {
        return new JsonResult(true, HttpStatus.OK.value(), data);
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
