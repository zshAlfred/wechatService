package com.zsh.wechat.api;

import feign.FeignException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ExceptionMapperHandler extends ResponseEntityExceptionHandler {

    protected final Log logger = LogFactory.getLog(this.getClass());

    @ExceptionHandler(Exception.class)
    public ResponseEntity<String> exceptionHandler(Exception exception) {
        logError(exception);
        return new ResponseEntity<>("Internal Server Error",
                HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<String> exceptionHandler(RuntimeException exception) {
        logError(exception);
        HttpStatus code = HttpStatus.INTERNAL_SERVER_ERROR;
        String message = exception.getMessage();
        if (exception instanceof FeignException) {
            code = HttpStatus.valueOf(((FeignException) exception).status());
            if (message.contains("; content:\n")) {
                String[] msgArray = message.split("; content:\n");
                message = msgArray[1];
            }
        }
        return new ResponseEntity(message, code);
    }

    private void logError(Exception exception) {
        String fullStackTrace = ExceptionUtils.getStackTrace(exception);
        logger.error("ResponseException exception: " + fullStackTrace);
    }
}
