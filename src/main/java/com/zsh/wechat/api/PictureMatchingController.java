package com.zsh.wechat.api;

import com.zsh.wechat.entity.MatchResult;
import com.zsh.wechat.service.CheatingProcessor;
import com.zsh.wechat.service.HotConfService;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by xinghao1 on 2017/6/8.
 */
@RequestMapping("/match")
@RestController
public class PictureMatchingController {

    private static final Logger logger = LoggerFactory.getLogger(PictureMatchingController.class);

    @Autowired
    CheatingProcessor cheatingProcessor;

    @Autowired
    HotConfService hotConfService;

    @RequestMapping(value = "/pic", method = RequestMethod.GET)
    @ApiOperation(value = "match existed cheating template", response = MatchResult.class)
    public MatchResult matchOne(@RequestParam("dir") String dir, @RequestParam("pic") String pic) {
        pic = pic.replace(".jpg", "");
        String file = dir + pic + ".jpg";
        MatchResult detect = cheatingProcessor.detect(file, hotConfService.getThreshold());
        return detect;
    }
}
