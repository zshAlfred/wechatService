package com.zsh.wechat.api;

import com.alibaba.fastjson.JSON;
import com.zsh.wechat.api.response.Token;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;


@RestController
public class TokenController {

    private static String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wx970ef907ae1c4590&secret=203a612f5dbfd74b8656e8d3387d4df5";

    private URI uri;
    private SimpleClientHttpRequestFactory requestFactory;

    @PostConstruct
    public void init() throws URISyntaxException {
        uri = new URI(url);
        requestFactory = new SimpleClientHttpRequestFactory();
    }


    //CHECKSTYLE:OFF
    @GetMapping("/token")
    @ApiOperation(value = "Get token", response = String.class)
    public String token(@RequestParam(name = "appid", required = false) String appid) {
        try {
            ClientHttpRequest chr = requestFactory.createRequest(uri, HttpMethod.GET);
            //chr.getBody().write(param.getBytes());//body中设置请求参数
            ClientHttpResponse res = chr.execute();
            InputStream is = res.getBody(); //获得返回数据,注意这里是个流
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder builder = new StringBuilder();
            String str;
            while ((str = br.readLine()) != null) {
                builder.append(str);
            }
            Token token = JSON.parseObject(builder.toString(), Token.class);
            return token.getAccessToken();
        } catch (Exception e) {
            return e.getMessage();
        }
    }
    //CHECKSTYLE:ON
}
