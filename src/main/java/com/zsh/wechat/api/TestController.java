package com.zsh.wechat.api;

import com.google.common.collect.ImmutableMap;
import com.zsh.wechat.constants.ConstVariableForRedis;
import com.zsh.wechat.entity.Material;
import com.zsh.wechat.service.DownloadFileService;
import com.zsh.wechat.service.RedisService;
import com.zsh.wechat.service.impl.FileCompressService;
import com.zsh.wechat.service.impl.ScrawlerService;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequest;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;



@RequestMapping("/test")
@RestController
public class TestController {

    private static String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wx970ef907ae1c4590&secret=203a612f5dbfd74b8656e8d3387d4df5";

    private URI uri;
    private SimpleClientHttpRequestFactory requestFactory;

    @PostConstruct
    public void init() throws URISyntaxException {
        uri = new URI(url);
        requestFactory = new SimpleClientHttpRequestFactory();
    }

    @GetMapping("/1")
    public String test1() {
        return "test method";
    }

    @PostMapping("/2")
    public String test2() {
        System.out.println("Hello world!");
        throw new RuntimeException("test exception");
    }

    @RequestMapping("/3")
    public Map test3() {
        return ImmutableMap.of("test", "json", "222", 22);
    }

    @Deprecated
    @RequestMapping("/4")
    public String getFileLink(){
        try {
            ClientHttpRequest chr = requestFactory.createRequest(uri, HttpMethod.GET);
            //chr.getBody().write(param.getBytes());//body中设置请求参数
            ClientHttpResponse res = chr.execute();
            InputStream is = res.getBody(); //获得返回数据,注意这里是个流
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            StringBuilder builder = new StringBuilder();
            String str;
            while ((str = br.readLine()) != null) {
                builder.append(str);
            }
//            String pageString=builder.toString();
//            int beginIx = buf.indexOf("查询结果[");
//            int endIx = buf.indexOf("上面四项依次显示的是");
//            String result = buf.substring(beginIx, endIx);

//            Token token = JSON.parseObject(builder.toString(), Token.class);
//            return token.getAccessToken();
            return "OK, done!";
        } catch (Exception e) {
            return e.getMessage();
        }
    }



    @RequestMapping("/6")
    public String spider(){
        try{
            scrawlerService.grabLinksFromWeb();
        }catch (Exception e){

        }
        return "done";
    }

    @RequestMapping("/7")
    public String testDownloadFile(){
        try{
            downloadFileService.downloadFile();
        }catch (Exception e){

        }

        return "done";
    }

    @RequestMapping("/8")
    public String testZip(){
        try{
            fileCompressService.testUZip();
        }catch (Exception e){
        }
        return "Done";
    }
    @RequestMapping("/9")
    public String testRedis(){
        try{
            for(int i=0; i<20;i++){
                String itemString=redisService.lpop(ConstVariableForRedis.RedisKeyUnhandledLinkList);
                ObjectMapper mapper=new ObjectMapper();
                Material item=mapper.readValue(itemString,Material.class);
                String fileName=item.getFileName();
                long fileSize=item.getFileSize();
            }
        }catch (Exception e){
            e.getStackTrace();
        }
        return "done";
    }

    @Autowired
    FileCompressService fileCompressService;

    @Autowired
    private DownloadFileService downloadFileService;

    @Autowired
    private ScrawlerService scrawlerService;

    @Autowired
    RedisService redisService;
}
