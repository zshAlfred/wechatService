package com.zsh.wechat.agent;

import com.zsh.wechat.agent.value_object.CTO;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Map;

@FeignClient(name = "product-service", url = "${services.product.baseUrl}")
public interface ProductServiceAgent {

    @Cacheable(cacheNames = "getCto")
    @RequestMapping(method = RequestMethod.GET, value = "/ctos/{country}/{ctoCode}")
    CTO getCto(@PathVariable("country") String country, @PathVariable("ctoCode") String ctoCode,
               @RequestParam("language") String language, @RequestParam("currency") String currency);

    @RequestMapping(method = RequestMethod.GET, value = "/ctos/wc/{country}/{ctoCode}")
    CTO getCtoWithoutCondition(@PathVariable("country") String country, @PathVariable("ctoCode") String ctoCode,
                               @RequestParam("language") String language, @RequestParam("currency") String currency);

    @Cacheable(cacheNames = "getCtoDescriptions")
    @RequestMapping(method = RequestMethod.GET, value = "/ctos/descriptions")
    Map<String, String> getCtoDescriptions(@RequestParam("codes") List<String> codes);

}
