package com.zsh.wechat.agent.value_object;

public class CTO {
    private String country;
    private String ctoCode;
    private String announceDate;
    private String withdrawnDate;
    private String endOfServiceDate;
    private String machineType;
    private String category;
    private String description;
    private double price;
    private boolean withDrawn;
    private String marketInfo;
    private String phCode;
    private String type;
    private boolean showPrice = true; // 1:show   0:not show

    public CTO() {
    }

    public CTO(String ctoCode, String category, boolean withdrawn) {
        this.ctoCode = ctoCode;
        this.category = category;
        this.withDrawn = withdrawn;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCtoCode() {
        return ctoCode;
    }

    public void setCtoCode(String ctoCode) {
        this.ctoCode = ctoCode;
    }

    public String getAnnounceDate() {
        return announceDate;
    }

    public void setAnnounceDate(String announceDate) {
        this.announceDate = announceDate;
    }

    public String getWithdrawnDate() {
        return withdrawnDate;
    }

    public void setWithdrawnDate(String withdrawnDate) {
        this.withdrawnDate = withdrawnDate;
    }

    public String getEndOfServiceDate() {
        return endOfServiceDate;
    }

    public void setEndOfServiceDate(String endOfServiceDate) {
        this.endOfServiceDate = endOfServiceDate;
    }

    public String getMachineType() {
        return machineType;
    }

    public void setMachineType(String machineType) {
        this.machineType = machineType;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public boolean isWithdrawnDateInvalid(String currentDate) {
        return withdrawnDate != null && currentDate.compareTo(withdrawnDate) >= 0;
    }

    public boolean isAnnounceDateInvalid(String currentDate) {
        return announceDate != null && announceDate.compareTo(currentDate) > 0;
    }

    private boolean isEndOfServiceDateInvalid(String currentDate) {
        return endOfServiceDate != null && currentDate.compareTo(endOfServiceDate) >= 0;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isWithDrawn() {
        return withDrawn;
    }

    public void setWithDrawn(boolean withDrawn) {
        this.withDrawn = withDrawn;
    }

    public String getMarketInfo() {
        return marketInfo;
    }

    public void setMarketInfo(String marketInfo) {
        this.marketInfo = marketInfo;
    }

    public String getPhCode() {
        return phCode;
    }

    public void setPhCode(String phCode) {
        this.phCode = phCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isShowPrice() {
        return showPrice;
    }

    public void setShowPrice(boolean showPrice) {
        this.showPrice = showPrice;
    }
}
