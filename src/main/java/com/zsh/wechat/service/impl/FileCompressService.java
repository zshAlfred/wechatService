package com.zsh.wechat.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zsh.wechat.constants.ConstVariableForRedis;
import com.zsh.wechat.entity.Material;
import com.zsh.wechat.service.RedisService;
import com.zsh.wechat.util.ZipUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by Lenovo on 7/22/2017.
 */
@Service
public class FileCompressService {

    private static final Logger logger = LoggerFactory.getLogger(FileCompressService.class);
    @Autowired
    RedisService redisService;

    public void testUZip() throws Exception{
        String itemString=redisService.lpop(ConstVariableForRedis.RedisKeySuccessDownloadList);
        ObjectMapper objectMapper=new ObjectMapper();
        Material item = objectMapper.readValue(itemString,Material.class);

        String filePath=ConstVariableForRedis.DownloadZipFileFolder+"\\"+item.getFileName();
        ZipUtil.unzip(filePath);
    }

}
