package com.zsh.wechat.service.impl;

import com.zsh.wechat.constants.ConstVariableForRedis;
import com.zsh.wechat.entity.FileListBean;
import com.zsh.wechat.entity.Material;
import com.zsh.wechat.service.RedisService;
import com.zsh.wechat.util.ScrawlerUtil;
import org.apache.xpath.SourceTree;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.lang.reflect.Executable;
import java.util.Date;

/**
 * Created by Lenovo on 7/22/2017.
 */
@Service
public class ScrawlerService {
    private static final Logger logger = LoggerFactory.getLogger(ScrawlerService.class);

    @Autowired
    RedisService redisService;

    @Value("${debug_mode.debugModeEnable}")
    private String debugModeEnable;

    ObjectMapper jsonMapper=new ObjectMapper();


    public void grabLinksFromWeb() throws Exception {

        logger.info("=========>Start to grab zip file links from web and process them");

        ScrawlerUtil scrawler = new ScrawlerUtil();

        FileListBean fileList = scrawler.getMaterialFileListBean();
        String fileNameArray[] = fileList.getFilelist().split(",");
        String fileSizeArray[] = fileList.getFilesize().split(",");

        //Get the flag, which indicate the newest record in the queue. If no flag has been stored( usually caused by the exception or shutdown),
        // new flag will be specified from the end of the queue. After generating the new flag, right push the popped up record.
        String flag = redisService.lpop(ConstVariableForRedis.RedisKeyUnHandledLinkFlag);
        if(flag==null){
            String itemString=redisService.rpop(ConstVariableForRedis.RedisKeyUnhandledLinkList);
            if(itemString!=null)
            {
                logger.info("There is no flag in the flag storage, rpop the latest one in the list and set it as the flag, then rpush it back to the list. The flag is "+flag+"\n\n");
                try{
                    Material item=jsonMapper.readValue(itemString,Material.class);
                    flag=item.getFileName();
                    redisService.rpush(ConstVariableForRedis.RedisKeyUnhandledLinkList,item);
                }catch (Exception e){
                    logger.error("Exception caught when try to parse json string within Scrawler service ===> "+e.getMessage());
                    throw new Exception("Exception caught when try to parse json string in Scrawler service");
                }

            }
        }
        else{
            flag=flag.substring(flag.indexOf('\"')+1,flag.lastIndexOf('\"'));
        }
        logger.info("The old flag is "+flag);

        String newFlag = flag;
        int count=0;

        for (int i = 0; i < fileNameArray.length; i++) {
            boolean newStart = false;
            if (i == 0 && flag == null) {
                flag = fgitileNameArray[0];
                newStart = true;
                logger.info("= = = = => The first time the program start, set the original flag as: "+flag);
            }
            if(debugModeEnable!=null&&debugModeEnable.equalsIgnoreCase("true"))
            {
                logger.info("***IsNewStart = "+newStart+"*******sort number: "+i+"*******"+fileNameArray[i]+"******"+fileNameArray[i].compareToIgnoreCase(flag));
            }

            if (newStart || fileNameArray[i].compareToIgnoreCase(flag) > 0) {
                newFlag = fileNameArray[i];
                try {
                    Material item = new Material();
                    item.setFileName(fileNameArray[i]);
                    item.setFileSize(Long.parseLong(fileSizeArray[i]));
                    redisService.rpush(ConstVariableForRedis.RedisKeyUnhandledLinkList, item);
                    count++;
                } catch (Exception e) {
                    logger.error("Exception caught when try to add new link record to redis. Message: \n\n"+e.getMessage()+"\n\n"+e.getStackTrace());
                }
            }
        }
        if(count>0)
        {
            logger.info("New records have been inserted into Redis, the count is "+ count);
            logger.info("New flag has been set: "+newFlag);
        }
        redisService.rpush(ConstVariableForRedis.RedisKeyUnHandledLinkFlag, newFlag);
        logger.info("==========>Successfully process all the links grabbed from web. The count of total record from web is "+fileNameArray.length+", the count of new record added is "+count);
    }
}
