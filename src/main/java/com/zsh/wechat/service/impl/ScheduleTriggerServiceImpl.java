package com.zsh.wechat.service.impl;

import com.zsh.wechat.service.DownloadFileService;
import com.zsh.wechat.service.ScheduleTriggerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


/**
 * Created by admin on 2017/6/19.
 */
@Component
public class ScheduleTriggerServiceImpl implements ScheduleTriggerService {

    private static final Logger logger = LoggerFactory.getLogger(ScheduleTriggerServiceImpl.class);

    @Autowired
    ScrawlerService scrawlerService;

    @Autowired
    DownloadFileService downloadFileService;

    @Value("${timers.scrawler_timer}")
    private static final String scrawler_timer="0 0/1 * * * ?";

    @Value("${timers.unzip_timer}")
    private static final String unzip_timer="0 2/20 * * * ?";

    @Value("${timers.download_timer}")
    private static final String download_timer="0 1/2 * * * ?";

    @Value("${timers.machineRecognition_timer}")
    private static final String machineRecognition_timer="0 0/1 * * * ?";

    @Value("${timers.zip_timer}")
    private static final String zip_timer="0 2/20 * * * ?";

    @Value("${debug_mode.debugModeEnable}")
    private String debugModeEnable;


    @Override
    @Scheduled(cron=scrawler_timer)
    public void triggerScrawler(){
        try{
            scrawlerService.grabLinksFromWeb();
            System.out.println("triggerScrawler method called!");
        }catch (Exception e) {
            logger.error("Exception caught when run the scrawler. "+e.getStackTrace());
        }
    }
    @Override
    @Scheduled(cron=unzip_timer)
    public void triggerUnzip(){

    }

    @Override
    @Scheduled(cron=download_timer)
    public void triggerDownloadFile(){
        if(downloadFileService.getDownloadOver())
        {
            try{
                downloadFileService.downloadFile();
            }catch (Exception e){
                logger.error("Exception caught when try to call download method.");
                logger.error(e.getMessage());
            }

        }
        else {
            if(debugModeEnable!=null&&debugModeEnable.equalsIgnoreCase("true")){
                logger.info("Download is running.");
            }
        }

    }

    @Override
    @Scheduled(cron=machineRecognition_timer)
    public void triggerMachineRecognition(){

    }

    @Override
    @Scheduled(cron=zip_timer)
    public void triggerZip(){

    }
}
