package com.zsh.wechat.service;

import com.zsh.wechat.constants.RedisKeys;
import com.zsh.wechat.entity.MatchResult;
import com.zsh.wechat.entity.Pic;
import com.zsh.wechat.model.CheatingTemplate;
import com.zsh.wechat.service.detecters.TemplateMatchDetecter;
import org.opencv.imgcodecs.Imgcodecs;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by xinghao1 on 2017/6/11.
 * 实现作弊图片的检测和分离
 */
@Service
public class CheatingProcessor {

    private static final Logger logger = LoggerFactory.getLogger(CheatingProcessor.class);
    @Autowired
    TemplateMatchDetecter templateMatchDetecter;

    @Autowired
    RedisKeys redisKeys;

    private List<CheatingTemplate> cheatingTemplates;

    @Value("${image_file.result_dir.cheat}")
    private String cheatDir;

    @Value("${image_file.result_dir.wait_manual}")
    private String waitManualDir;

    @Value("${image_file.cheating_templates}")
    private String cheatingTemplatePath;

    @Autowired
    RedisService redisService;

    @PostConstruct
    public void init() {
        if (cheatingTemplatePath.startsWith("classpath:")) {
            cheatingTemplatePath = getClass().getResource(cheatingTemplatePath.replace("classpath:", "")).getPath();
        }
        loadCheatingTemplates();
        logger.info("image_file.cheating_templates={}", cheatingTemplatePath);
        logger.info("image_file.result_dir.wait_manual={}", waitManualDir);
        logger.info("image_file.result_dir.cheat={}", cheatDir);
    }

    public List<CheatingTemplate> loadCheatingTemplates() {
        File cheatingTemplateDir = new File(cheatingTemplatePath);
        cheatingTemplates = Arrays.stream(cheatingTemplateDir.listFiles())
                .map(cheatingPic -> {
                    logger.info("添加作弊模板{}", cheatingPic.getAbsolutePath());
                    return new CheatingTemplate(cheatingPic.getAbsolutePath(), Imgcodecs.imread(cheatingPic.getAbsolutePath()));
                })
                .collect(Collectors.toList());
        logger.info("load cheating templates:{}", cheatingTemplates);
        return cheatingTemplates;
    }

    public MatchResult detect(String sourceFile, double threshold) {
        try {
            Optional<MatchResult> any = cheatingTemplates.parallelStream()
                    .map(entry -> {
                        MatchResult r = templateMatchDetecter.getMatchResult(sourceFile, entry.getTemplate());
                        return r.setTarget(entry.getName());
                    })
                    .filter(result -> result.matchByThreshold(threshold))
                    .findAny();
            if (any.isPresent()) {
                return any.get().setSource(sourceFile);
            }
        } catch (Exception e) {
            logger.error("detect异常", e);
        }
        return MatchResult.noMatch(sourceFile);
    }

    public boolean processResult(MatchResult result) {
        if (result.isMatch()) {
            return processCheat(result.getSource());
        }
        return processUnCheat(result.getSource());
    }

    public boolean processCheat(String file) {
        try {
            File from = new File(file);
            String fullPath = waitManualDir + from.getName();
            File to = new File(fullPath);
            from.renameTo(to);
            Pic pic = Pic.of(to);
            redisService.rpush(redisKeys.systemDetectedCheatingList, pic);
            return true;
        } catch (Exception e) {
            logger.error("move file exception" + file, e);
            return false;
        }
    }

    public boolean confirmCheat(String file) {
        try {
            File from = new File(file);
            String fullPath = cheatDir + from.getName();
            File to = new File(fullPath);
            return from.renameTo(to);
        } catch (Exception e) {
            logger.error("move confirmed cheat pic failed:" + file, e);
            return false;
        }
    }

    public boolean processUnCheat(String file) {
        try {
            File from = new File(file);
            return from.delete();
        } catch (Exception e) {
            logger.error("delete file exception:" + file, e);
            return false;
        }
    }


}
