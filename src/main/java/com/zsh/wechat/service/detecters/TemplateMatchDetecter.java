package com.zsh.wechat.service.detecters;

import com.zsh.wechat.entity.MatchResult;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

import static com.zsh.wechat.infrastructure.TogglzConfig.FeatureToggle.draw_match_prosition;

/**
 * Created by xinghao1 on 2017/6/9.
 * 使用matchTemplate方式查找与目标图像最为匹配的子图:
 * 在输入源图像Sourceimage（I）中滑动框，寻找各个位置与模板图像Template image（T）的相似度，并将结果保存在结果矩阵result matrix（R）中。该矩阵的每一个点的亮度表示与模板T的匹配程度。然后可以通过函数minMaxLoc定位矩阵R中的最大值（该函数也可以确定最小值）。那通过什么去评价两个图像相似呢？这就存在一个评价准则，也就是参数method，它可以有以下值（匹配的方法）：
 * CV_TM_SQDIFF 平方差匹配法，最好的匹配为0，值越大匹配越差；
 * CV_TM_SQDIFF_NORMED 归一化平方差匹配法；
 * CV_TM_CCORR 相关匹配法，采用乘法操作，数值越大表明匹配越好；
 * CV_TM_CCORR_NORMED 归一化相关匹配法；
 * CV_TM_CCOEFF 相关系数匹配法，最好的匹配为1，-1表示最差的匹配；
 * CV_TM_CCOEFF_NORMED 归一化相关系数匹配法；
 * 前面两种方法为越小的值表示越匹配，后四种方法值越大越匹配。
 */
@Service
public class TemplateMatchDetecter {

    private static final Logger logger = LoggerFactory.getLogger(TemplateMatchDetecter.class);

    @Autowired
    private Environment env;

    private String testOutputDir = "D:\\test\\out\\";

    private short index = 1;

    private int cvType;

    @PostConstruct
    public void init() {
        initCvType();
        initOpenCvLib();
    }

    private void initCvType() {
        String arch = System.getProperty("os.arch");
//        if (arch.contains("64")) {
//            cvType = CvType.CV_64FC1;
//        } else {
            cvType = CvType.CV_32FC1;
//        }
    }

    public void initOpenCvLib() {
        String openCvLib = getOpenCvLib();
        logger.info("加载到opencv 库{}", openCvLib);
        System.load(openCvLib);
    }

    private String getOpenCvLib() {
        String arch = System.getProperty("os.arch");
        String path = null;
        if (arch.contains("64")) {
            path = env.getProperty("opencv.lib.x64");
        } else {
            path = env.getProperty("opencv.lib.x86");
        }
        if (path.startsWith("classpath:")) {
            path = getClass().getResource(path.replace("classpath:", "")).getPath();
        }
        return path;
    }


    public boolean isContain(String sourcePath, String dstPath) {
        Mat source = Imgcodecs.imread(sourcePath, 1);
        Mat dst = Imgcodecs.imread(dstPath);
        return isContain(source, dst);
    }

    public boolean isContain(Mat source, Mat dst) {
        //创建和原图相同的大小，储存匹配度
        Mat result = Mat.zeros(source.rows(), source.cols(), cvType);
        //调用模板匹配方法
        Imgproc.matchTemplate(source, dst, result, Imgproc.TM_SQDIFF);
        //规格化
        Core.normalize(result, result, 0, 1, Core.NORM_MINMAX, -1, new Mat());
        Imgproc.threshold(result, new Mat(), 0.8, 255, 0);
        //获得最可能点，MinMaxLocResult是其数据格式，包括了最大、最小点的位置x、y
        Core.MinMaxLocResult mlr = Core.minMaxLoc(result);
        Point matchLoc = mlr.minLoc;

//        double v = Imgproc.compareHist(source, dst, CV_COMP_INTERSECT);

        return matchLoc.x > 0d && matchLoc.y > 0d &&
            matchLoc.x < source.width() && matchLoc.y < source.height();
    }

    //开发测试匹配到的区域，线上不需要打开
    private void debugMatch(Mat source, Mat dst, Point matchLoc, String outputImage) {
        if (!draw_match_prosition.isActive()) {
            return;
        }
        //在source Mat圈出匹配位置
        Imgproc.rectangle(source, matchLoc, new Point(matchLoc.x + dst.width(), matchLoc.y + dst.height()), new Scalar(0, 255, 0));
        //写入到image文件
        Imgcodecs.imwrite(testOutputDir + (index++) + outputImage.substring(outputImage.lastIndexOf("\\") + 1), source);
    }

    public MatchResult getMatchResult(Mat source, Mat template) {
        int cols = source.cols() - template.cols() + 1;
        int rows = source.rows() - template.rows() + 1;
        Mat result = new Mat(cols, rows, cvType);
        Mat resultThreshold = new Mat(result.rows(), result.cols(), cvType);
        Mat logPolarSource = new Mat(source.rows(), source.cols(), cvType);
        Mat logPolarTemplate = new Mat(template.rows(), template.cols(), cvType);
        Imgproc.logPolar(source, logPolarSource, new Point(logPolarSource.cols() / 2, logPolarSource.rows() / 2), 0.1, Imgproc.CV_WARP_INVERSE_MAP + Imgproc.CV_WARP_FILL_OUTLIERS);
        Imgproc.logPolar(template, logPolarTemplate, new Point(logPolarTemplate.cols() / 2, logPolarTemplate.rows() / 2), 0.1, Imgproc.CV_WARP_INVERSE_MAP + Imgproc.CV_WARP_FILL_OUTLIERS);
        Imgproc.matchTemplate(logPolarSource, logPolarTemplate, result, Imgproc.TM_CCOEFF);
        Core.normalize(result, result, 0, 1, Core.NORM_MINMAX, -1, new Mat());
        Imgproc.threshold(result, resultThreshold, 0.8, 255, 0);
        int white = 0;
        for (int i = 0; i < resultThreshold.cols(); i++) {
            for (int j = 0; j < resultThreshold.rows(); j++) {
                if ((int) resultThreshold.get(j, i)[0] == 255) {
                    white++;
                }
            }
        }
        double percentWhite = 100d - (100.0 / ((double) resultThreshold.cols() * (double) resultThreshold.rows())) * (double) white;

        Point loc = Core.minMaxLoc(result).minLoc;
        return MatchResult.of(loc, percentWhite);
    }

    public MatchResult getMatchResult(String strSource, Mat template) {
        Mat source = Imgcodecs.imread(strSource);

        MatchResult matchResult = getMatchResult(source, template);
//        debugMatch(source, template, matchResult.getPoint(), strSource);
        return matchResult;
    }

    MatchResult getMatchResult(String strSource, String strTemplate) {
        Mat source = Imgcodecs.imread(strSource);
        Mat template = Imgcodecs.imread(strTemplate);
        MatchResult matchResult = getMatchResult(source, template);
//        debugMatch(source, template, matchResult.getPoint(), strSource);
        return matchResult;
    }

}
