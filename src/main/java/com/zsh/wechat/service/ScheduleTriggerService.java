package com.zsh.wechat.service;

/**
 * Created by admin on 2017/6/19.
 */
public interface ScheduleTriggerService {

    public void triggerScrawler();

    public void triggerDownloadFile();

    public void triggerUnzip();

    public void triggerMachineRecognition();

    public void triggerZip();
}
