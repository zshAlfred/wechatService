package com.zsh.wechat.service;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by xinghao1 on 2017/7/16.
 * todo 引入spring-conf 来避免这样写
 */
@Controller
public class HotConfService {

    //匹配度达到该阈值记为匹配成功
    private double threshold = 99.5;

    @GetMapping("/threshold")
    public double getThreshold() {
        return threshold;
    }

    @PutMapping("/update_threshold")
    public double setThreshold(@RequestParam("threshold") double threshold) {
        this.threshold = threshold;
        return threshold;
    }
}
