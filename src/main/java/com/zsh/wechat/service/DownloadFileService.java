package com.zsh.wechat.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zsh.wechat.constants.ConstVariableForRedis;
import com.zsh.wechat.entity.Material;
import com.zsh.wechat.util.BigFileDownloadManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * Created by admin on 2017/6/12.
 */

@Service
public class DownloadFileService {

    private static final Logger logger = LoggerFactory.getLogger(DownloadFileService.class);

    //Timer will poll this value to check if the current download has completed.
    //Note that it's not thread safe. There can be only one timer(thread) check it at long intervals.
    private boolean downloadOver=false;

    @Autowired
    RedisService redisService;

    @Value("${debug_mode.debugModeEnable}")
    private String debugModeEnable;

    private BigFileDownloadManager bfdm;

    ObjectMapper objectMapper;

    public DownloadFileService(){
        objectMapper=new ObjectMapper();
        bfdm=new BigFileDownloadManager();
    }
    public void downloadFile() throws Exception{
        setDownloadOver(false);
        //lpop a link from the list, note that the link maybe have been handled
        String itemString=redisService.lpop(ConstVariableForRedis.RedisKeyUnhandledLinkList);
        if(itemString==null)
        {
            setDownloadOver(true);
            return;
        }
        try{
            ObjectMapper objectMapper=new ObjectMapper();
            Material item = objectMapper.readValue(itemString,Material.class);

            long start = System.currentTimeMillis();

            boolean success=bfdm.doDownload(ConstVariableForRedis.FileURL+item.getFileName());

            long end = System.currentTimeMillis();
            if(debugModeEnable!=null&&debugModeEnable.equalsIgnoreCase("true")){
                logger.info("===============The time consumed to download "+item.getFileName()+" is ：" + (end - start) + "===============");
            }

            if(success){
                redisService.rpush(ConstVariableForRedis.RedisKeySuccessDownloadList,itemString);
            }
        }catch (Exception ex){
            logger.error("Exception caught when try to parse an unhandled link record to object and download the zip file.");
            logger.error(ex.getMessage()+"\n\n"+ ex.getStackTrace().toString());
            //Add the failed record to Redis
            redisService.rpush(ConstVariableForRedis.RedisKeyFailedDownloadList,itemString);
        }
        finally {
            setDownloadOver(true);
        }
    }

    public void setDownloadOver(boolean downloadOver){
        this.downloadOver=downloadOver;
    }
    public boolean getDownloadOver(){
        return this.downloadOver;
    }
}
