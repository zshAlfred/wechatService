package com.zsh.wechat.infrastructure;

import feign.Response;
import org.apache.commons.lang3.StringUtils;

public class FeignResponseException extends RuntimeException {

    private Response response;

    public FeignResponseException(Response response) {
        this.response = response;
    }

    public Response getResponse() {
        return response;
    }

    @Override
    public String getMessage() {
        String message = super.getMessage();
        if (StringUtils.isEmpty(message)) {
            return response.toString();
        }
        return message;
    }
}
