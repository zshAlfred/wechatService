package com.zsh.wechat.infrastructure;

import com.zsh.wechat.entity.ModeConfig;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Lenovo on 8/13/2017.
 */
@Configuration
public class YmlConfig {

    @Bean(name="modeConfig")
    @ConfigurationProperties(prefix = "debug_mode")
    public ModeConfig getModeConfig() {
        ModeConfig config = new ModeConfig();
        return config;
    }
}
