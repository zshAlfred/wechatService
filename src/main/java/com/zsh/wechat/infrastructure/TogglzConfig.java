package com.zsh.wechat.infrastructure;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.togglz.core.Feature;
import org.togglz.core.annotation.EnabledByDefault;
import org.togglz.core.annotation.Label;
import org.togglz.core.context.FeatureContext;
import org.togglz.core.manager.EnumBasedFeatureProvider;
import org.togglz.core.spi.FeatureProvider;
import org.togglz.core.user.NoOpUserProvider;

@Configuration
public class TogglzConfig {

//    @Value("${togglz.state-repository.mongoDB.client-URI}")
//    private String mongoDbClientURI;
//
//    @Value("${togglz.state-repository.mongoDB.dbname}")
//    private String mongoDbName;
//
//    @Bean
//    @Profile({"!offline"})
//    public StateRepository stateRepository() {
//        MongoClient mongoClient = new MongoClient(new MongoClientURI(mongoDbClientURI));
//        return MongoStateRepository.newBuilder(mongoClient, mongoDbName)
//            .writeConcern(WriteConcern.ACKNOWLEDGED)
//            .build();
//    }

    @Bean
    public FeatureProvider featureProvider() {
        return new EnumBasedFeatureProvider(FeatureToggle.class);
    }

    @Bean
    public NoOpUserProvider userProvider() {
        return new NoOpUserProvider();
    }

    public enum FeatureToggle implements Feature {

//        @EnabledByDefault
        @Label("First Feature")
        draw_match_prosition,;


        public boolean isActive() {
            return FeatureContext.getFeatureManager().isActive(this);
        }
    }
}
