package com.zsh.wechat.infrastructure;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class SpringBeanUtil implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    public static Object getBeanByName(String beanId) {
        if (beanId == null) {
            return null;
        }

        return applicationContext.getBean(beanId);

    }

    public static Object getBeanByType(Class clazz) {
        if (clazz == null) {
            return null;
        }
        return applicationContext.getBean(clazz);
    }

    @Override
    public void setApplicationContext(ApplicationContext ac) {
        this.applicationContext = ac;
    }
}
