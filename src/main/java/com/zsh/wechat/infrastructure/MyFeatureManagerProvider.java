package com.zsh.wechat.infrastructure;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.togglz.core.manager.FeatureManager;
import org.togglz.core.spi.FeatureManagerProvider;


public class MyFeatureManagerProvider implements FeatureManagerProvider {

    private static Log log = LogFactory.getLog(MyFeatureManagerProvider.class);

    @Override
    public int priority() {
        return 11;
    }

    @Override
    public FeatureManager getFeatureManager() {
        return (FeatureManager) SpringBeanUtil.getBeanByType(FeatureManager.class);
    }

}
