package com.zsh.wechat;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Created by xinghao1 on 2017/6/26.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ImageFilterApplication.class})
@Ignore
public class SpringBaseTest {
}
