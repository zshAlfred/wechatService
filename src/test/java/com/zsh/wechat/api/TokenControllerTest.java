package com.zsh.wechat.api;

import com.zsh.wechat.message.NotifyService;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.Base64Utils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.isNotNull;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by xinghao1 on 2017/5/25.
 */
@Ignore
public class TokenControllerTest {
    @InjectMocks
    TokenController tokenController;

    @Mock
    NotifyService notifyService;

    private MockMvc mockMvc;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(tokenController).build();
    }

    @Test
    public void should_retrieve_token_given_param_is_correct_when_call_token() throws Exception {
        when(notifyService.testMethod(anyString())).thenReturn("testString");

        String auth = new String(Base64Utils.encode("lbp:lbp".getBytes()));
        String responseString = mockMvc.perform(
            get("/token")
                .param("username", "testUser")
                .param("role", "test")
                .param("grant_type", "password")
                .header("Authorization", auth)
        ).andExpect(status().isOk())
            .andReturn().getResponse().getContentAsString();

//        assertThat(responseString, isEmptyOrNullString());
    }

}