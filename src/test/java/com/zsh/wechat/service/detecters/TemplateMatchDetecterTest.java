package com.zsh.wechat.service.detecters;

import com.zsh.wechat.entity.MatchResult;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by xinghao1 on 2017/6/9.
 */
@Ignore
@RunWith(MockitoJUnitRunner.class)
public class TemplateMatchDetecterTest {

    @InjectMocks
    TemplateMatchDetecter templateMatchDetecter;


    @Before
    public void setup() {
        String arch = System.getProperty("os.arch");
        if (arch.equals("x64")) {
            System.load(getClass().getResource("/opencv_libs/320/x64/opencv_java320.dll").getPath());
        } else if (arch.equals("x86")) {
            System.load(getClass().getResource("/opencv_libs/320/x86/opencv_java320.dll").getPath());
        } else {
            throw new RuntimeException("load openCV lib failed");
        }
    }

    @Test
    public void should_match_template_from_origin_picture() {
        String source = "D:/test/zhunxin_source1.jpg";
        String dst = "D:/test/1.png";

//        when(openCVService.isContain(anyString(), anyString())).thenCallRealMethod();

        boolean contain = templateMatchDetecter.isContain(source, dst);

        assertThat(contain);
    }

    @Test
    public void should_match_zhunxin() {
        String source = "D:/test/zhunxin_source1.jpg";

        String dir = "D:/test/templates/";

        MatchResult matchResult = templateMatchDetecter.getMatchResult(source, "D:/test/1.png");
        System.out.println(matchResult);

        matchResult = templateMatchDetecter.getMatchResult(source, dir + "zhunxin1_1.png");
        System.out.println(matchResult);
        matchResult = templateMatchDetecter.getMatchResult(source, dir + "zhunxin1_2.png");
        System.out.println(matchResult);

        matchResult = templateMatchDetecter.getMatchResult(source, dir + "zhunxin1_3.png");
        System.out.println(matchResult);

        matchResult = templateMatchDetecter.getMatchResult(source, dir + "zhunxin2_1.png");
        System.out.println(matchResult);

        matchResult = templateMatchDetecter.getMatchResult(source, dir + "zhunxin2_2.png");
        System.out.println(matchResult);

        matchResult = templateMatchDetecter.getMatchResult(source, dir + "zhunxin2_3.png");
        System.out.println(matchResult);
    }

    @Test
    public void should_match_kuang() {

        MatchResult matchResult = templateMatchDetecter.getMatchResult(
            "D:/test/ccc.png", "D:/test/kuang.png");
        System.out.println(matchResult);

        matchResult = templateMatchDetecter.getMatchResult(
            "D:/test/zhunxin_source1.jpg", "D:/test/kuang.png");
        System.out.println(matchResult);
        matchResult = templateMatchDetecter.getMatchResult(
            "D:/test/toushi_part.png", "D:/test/kuang.png");
        System.out.println(matchResult);

        matchResult = templateMatchDetecter.getMatchResult(
            "D:/test/toushi1.jpg", "D:/test/kuang.png");
        System.out.println(matchResult);
    }

    @Test
    public void should_match_toushiren() {

        MatchResult matchResult = templateMatchDetecter.getMatchResult(
            "D:/test/ccc.png", "D:/test/templates/temp.png");
        System.out.println(matchResult);

        matchResult = templateMatchDetecter.getMatchResult(
            "D:/test/zhunxin_source1.jpg", "D:/test/templates/temp.png");
        System.out.println(matchResult);
        matchResult = templateMatchDetecter.getMatchResult(
            "D:/test/toushi_part.png", "D:/test/templates/temp.png");
        System.out.println(matchResult);

        matchResult = templateMatchDetecter.getMatchResult(
            "D:/test/toushi1.jpg", "D:/test/templates/temp.png");
        System.out.println(matchResult);
    }


}