package com.zsh.wechat.service.detecters;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

import java.io.File;

/**
 * Created by xinghao1 on 2017/6/9.
 */
@Ignore
public class CheatingOCRDetecterTest {

    @InjectMocks
    CheatingOCRDetecter cheatingOcrDetecter;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void test() throws Exception {

        File testDataDir = new File(getClass().getResource("/ocr_cheating").getPath());
        System.out.println(testDataDir.listFiles().length);
        int i = 0;
        for (File file : testDataDir.listFiles()) {
            i++;
            String recognizeText = cheatingOcrDetecter.recognizeText(file);
            System.out.print(recognizeText + "\t");

            if (i % 5 == 0) {
                System.out.println();
            }
        }
    }

    @Test
    public void should_return_osArch(){
        String arch = System.getProperty("os.arch");
        System.out.println(arch);
    }

}