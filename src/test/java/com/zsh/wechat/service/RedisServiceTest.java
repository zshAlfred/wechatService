package com.zsh.wechat.service;

import com.zsh.wechat.ImageFilterApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by xinghao1 on 2017/7/21.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {ImageFilterApplication.class})
public class RedisServiceTest {

    @Autowired
    RedisService redisService;


    @Test
    public void rpush() throws Exception {
        redisService.rpush("test:rpush","test data");
        String lpop = redisService.lpop("test:rpush");
        assertThat(lpop.equals("test data"));
    }

}