#! /bin/sh -e

java ${JAVA_OPTS}  -jar $(ls /work/*.jar) ${PROJECT_OPTS} --spring.profiles.active=${PROFILE}
