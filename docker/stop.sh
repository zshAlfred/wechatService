#! /bin/sh -e

# notify eureka I'm shutting down
pid=$(pgrep -f 'java')
if [ -n "${pid}" ];then
        kill ${pid}
fi